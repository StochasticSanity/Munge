# Munge
A password munger inspired by [Th3S3cr3tAg3nt/Munge](https://github.com/Th3S3cr3tAg3nt/Munge).

**If it already exists, why did you remake it?**

**Python3**
- Th3S3cr3tAg3nt version was made 6 years ago in Python 2!

**Performance**
- My script is built with performance in mind. It can go through 50k passwords in 6 seconds and has approximately 2X the edits (around 4M) compared to the Th3S3cr3tAg3nt version (approximately 2M).

**Why Not?**
- Why not? It was fun, wasn't it?
